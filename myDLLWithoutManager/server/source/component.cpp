#include "component.h"
using namespace std;

IID Constants::IID_IUnknown = {0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};
IID Constants::IID_IClassFactory = {0x00000001,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};

IID Constants::IID_IDefaultMatrix = {0x00000001,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IProMatrix = {0x00000002,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IMatrixClassFactory = {0x00000011,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};

//781C3DFD-8C32-4991-A6BC-5212FEB952B1
CLSID Constants::CLSID_Server = {0x781C3DFD,0x8C32,0x4991,{0xA6,0xBC,0x52,0x12,0xFE,0xB9,0x52,0xB1}};



Server::Server() {
  cout << "Server::Server" << endl;
  refCount = 0;
}

HRESULT Server::QueryInterface(const IID& iid, void** object){
   cout << "Server QueryInterface" << endl;

   if (iid == Constants::IID_IUnknown)
   {
     *object = (IUnknown*) (IDefaultMatrix*) this;
   }
   else if (iid == Constants::IID_IDefaultMatrix)
   {
     *object = static_cast<IDefaultMatrix*>(this);
   }
   else if (iid == Constants::IID_IProMatrix)
   {
     *object = static_cast<IProMatrix*>(this);
   }
   else
   {
     *object = NULL;
     return E_NOINTERFACE;
   }

   this->AddRef();
   return S_OK;
}

ULONG Server::AddRef(){
    refCount++;
    return refCount;
}

ULONG Server::Release(){
    refCount--;
    if (refCount == 0)
    {
        delete this;
    }
    
    return refCount;
}

HRESULT Server::find_determinant() {	 	    		
  return S_OK;
}

// HRESULT Server::set_matrix() 
// {	 	    		
//   cout << "Server set matrix" << endl;
//   cin >> M;
//   cin >> N;
// 	int matrix [M][N];
//   for (int i = 0; i < N; i++) {
// 		for (int j = 0; j < M; j++)
// 			cin >> matrix[i][j];
// 	};
// 	for (int i = 0; i < N; i++) {
// 		for (int j = 0; j < M; j++){
// 			cout << matrix[i][j];
// 			};
// 		cout << " " << endl;
// 	}; 	   
// 	return matrix[M][N];
//   //return S_OK;
// }

HRESULT Server::mult_num() {
  int N;
  int M;
  int num;
	cin >> num;
  int matrix[N][M];
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++)
			matrix[i][j] *= num;
	};
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++){
			cout << matrix[i][j];
			};
		cout << " " << endl;
	};
  return S_OK;
}

HRESULT Server::addition(){	 	    		
  cout << "Server::addition" << endl;
  return S_OK;
}

HRESULT Server::subtraction(){	 	    		
  return S_OK;
}

HRESULT Server::exponentiation(){	 	    		
  return S_OK;
}

HRESULT Server::transposition(){	 	    		
  return S_OK;
}

HRESULT Server::rank(){	 	    		
  return S_OK;
}

HRESULT Server::reverse(){	 	    		
  return S_OK;
}

HRESULT Server::minor(){	 	    		
  return S_OK;
}

Server::~Server(){
    cout<<"Server::~Server()"<<endl;
}

//DefaultMatrixClassFactory__________________________________________________________________________________

MatrixClassFactory::MatrixClassFactory(){
    cout << "MatrixClassFactory::MatrixClassFactory" << endl;
    this->refCount = 0;
    }


HRESULT MatrixClassFactory::QueryInterface(const IID& iid, void** object){
    cout<<"MatrixClassFactory::QueryInterface"<<endl;
        if (iid == Constants::IID_IClassFactory)
        {
          *object = (IUnknown*) (IClassFactory*) this;
        }
        else if (iid == Constants::IID_IMatrixClassFactory){
            *object = (IUnknown*) (IMatrixClassFactory*) this;
        }
        else{
            *object = NULL;
            return E_NOINTERFACE;
        }

        this->AddRef();
        return S_OK;
}

ULONG MatrixClassFactory::AddRef(){
    refCount++;
    return refCount;
}

ULONG MatrixClassFactory::Release(){
    refCount--;
    if (refCount == 0){
        delete this;
    }
    return refCount;
}

HRESULT MatrixClassFactory::CreateInstance(IUnknown* pUnknownOuter,const IID& iid, void** object){
    cout<<"MatrixClassFactory::CreateInstance"<<endl;

    if (pUnknownOuter != NULL){
        return E_NOTIMPL;
    }
    IUnknown* s = (IUnknown*) (IDefaultMatrix*) new Server();

    HRESULT res = s->QueryInterface(iid, object);

    return res;
}

HRESULT MatrixClassFactory::CreateInstance(const IID& iid, void** object, int license[]){
    cout<<"MatrixClassFactory::CreateInstance with license"<<endl;
    HRESULT res = E_NOTIMPL;

    if (license != NULL){
        IUnknown*s = (IUnknown*)(IDefaultMatrix*) new Server();

        res = s->QueryInterface(iid, object);
    }
    return res;
}

HRESULT MatrixClassFactory::LockServer(BOOL bLock){
    return S_OK;
}

MatrixClassFactory::~MatrixClassFactory(){
    cout<<"MatrixClassFactory::~MatrixClassFactory"<<endl;
}

#pragma once

#include "lib/interface.h"
#include "lib/interface2.h"

#include <windows.h>
#include <iostream>

class AServer
{
    private:
        const char* msg;
    public:
        AServer(const char* msg);
        const char* GetMessage();
};

class BServer
{
    private:
        IDefaultMatrix* dm;
        IProMatrix* pm;
        IOneMatrix* om; 
        BServer(const BServer&) {};
        BServer& operator=(const BServer&) {return *this;};
    public:
        BServer();
        BServer(int license[]);

        virtual HRESULT __stdcall setMatrix();

        virtual HRESULT __stdcall mult_num();
        virtual HRESULT __stdcall addition();
        virtual HRESULT __stdcall subtraction();
        virtual HRESULT __stdcall exponentiation();

        virtual HRESULT __stdcall transposition();
        virtual HRESULT __stdcall rank();
        virtual HRESULT __stdcall reverse();
        virtual HRESULT __stdcall minor();

        ~BServer();
};
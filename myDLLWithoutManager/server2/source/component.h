#pragma once

#include "interface2.h"
#include "lib/interface.h"

class MatrixManager: public IDefaultMatrix, public IProMatrix, public IOneMatrix{
    private:
        int refCount;
        IDefaultMatrix* dm;
        IProMatrix* pm;
    public:
        MatrixManager();

        virtual HRESULT __stdcall QueryInterface(const IID& iid, void** object);
        virtual ULONG __stdcall AddRef();
        virtual ULONG __stdcall Release();

        virtual HRESULT __stdcall mult_num();
        virtual HRESULT __stdcall addition();
        virtual HRESULT __stdcall subtraction();
        virtual HRESULT __stdcall exponentiation();

        virtual HRESULT __stdcall find_determinant();
        virtual HRESULT __stdcall transposition();
        virtual HRESULT __stdcall rank();
        virtual HRESULT __stdcall reverse();
        virtual HRESULT __stdcall minor();

        virtual HRESULT __stdcall setMatrix();

        ~MatrixManager();
};

class MatrixManagerClassFactory: public IClassFactory, public IMatrixManagerClassFactory{
    private:
        int refCount;
    public:
        MatrixManagerClassFactory();

        virtual HRESULT __stdcall QueryInterface(const IID& iid, void** object);
        virtual ULONG __stdcall AddRef();
        virtual ULONG __stdcall Release();

        virtual HRESULT __stdcall CreateInstance(IUnknown* pUnknownOuter, const IID& iid, void** object);
        virtual HRESULT __stdcall CreateInstance(const IID& iid, void** object, int license[]);

        virtual HRESULT __stdcall LockServer(BOOL bLock);

        ~MatrixManagerClassFactory();
};
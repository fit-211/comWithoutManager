#include "component.h"
using namespace std;

IID Constants::IID_IUnknown = {0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};
IID Constants::IID_IClassFactory = {0x00000001,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};

IID Constants::IID_IDefaultMatrix = {0x00000001,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IProMatrix = {0x00000002,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IMatrixClassFactory = {0x00000011,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IDispatch = {0x00020400,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};


//781C3DFD-8C32-4991-A6BC-5212FEB952B1
CLSID Constants::CLSID_Server = {0x781C3DFD,0x8C32,0x4991,{0xA6,0xBC,0x52,0x12,0xFE,0xB9,0x52,0xB1}};



Server::Server() {
  cout << "Server::Server" << endl;
  refCount = 0;
  check = 123;
}

HRESULT Server::QueryInterface(const IID& iid, void** object){
   cout << "Server QueryInterface" << endl;

   if (iid == Constants::IID_IUnknown)
   {
     *object = (IUnknown*) (IDefaultMatrix*) this;
   }
   else if (iid == Constants::IID_IDefaultMatrix)
   {
     *object = static_cast<IDefaultMatrix*>(this);
   }
   else if (iid == Constants::IID_IProMatrix)
   {
     *object = static_cast<IProMatrix*>(this);
   }
   else if (iid == Constants::IID_IDispatch)
    {
      *object = static_cast<IDispatch*>(this);
    }
   else
   {
     *object = NULL;
     return E_NOINTERFACE;
   }

   this->AddRef();
   return S_OK;
}

ULONG Server::AddRef(){
    refCount++;
    return refCount;
}

ULONG Server::Release(){
    refCount--;
    if (refCount == 0)
    {
        delete this;
    }
    
    return refCount;
}
//реализация
HRESULT Server::find_determinant() {	 	    		
  return S_OK;
}

// HRESULT Server::set_matrix() 
// {	 	    		
//   cout << "Server set matrix" << endl;
//   cin >> M;
//   cin >> N;
// 	int matrix [M][N];
//   for (int i = 0; i < N; i++) {
// 		for (int j = 0; j < M; j++)
// 			cin >> matrix[i][j];
// 	};
// 	for (int i = 0; i < N; i++) {
// 		for (int j = 0; j < M; j++){
// 			cout << matrix[i][j];
// 			};
// 		cout << " " << endl;
// 	}; 	   
// 	return matrix[M][N];
//   //return S_OK;
// }

HRESULT Server::mult_num() {
  int N;
  int M;
  int num;
	cin >> num;
  int matrix[N][M];
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++)
			matrix[i][j] *= num;
	};
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++){
			cout << matrix[i][j];
			};
		cout << " " << endl;
	};
  return S_OK;
}

HRESULT Server::addition(){	 	    		
  cout << "Server::addition" << endl;
  return S_OK;
}

HRESULT Server::subtraction(){	 	    		
  return S_OK;
}

HRESULT Server::exponentiation(){	 	    		
  return S_OK;
}

HRESULT Server::transposition(){	 	    		
  return S_OK;
}

HRESULT Server::rank(){	 	    		
  return S_OK;
}

HRESULT Server::reverse(){	 	    		
  return S_OK;
}

HRESULT Server::minor(){	 	    		
  return S_OK;
}
//IDispatch_start
HRESULT __stdcall Server::GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames,
                                    LCID lcid, DISPID* rgDispId)
{
    std::cout<<"Server::GetIDsOfNames"<<std::endl;

    if (cNames != 1) {return E_NOTIMPL;}

    if (wcscmp(rgszNames[0], L"mult_num") == 0)
    {
        rgDispId[0] = 1;
    }

    else if (wcscmp(rgszNames[0], L"addition") == 0)
    {
        rgDispId[0] = 2;
    }

    else if (wcscmp(rgszNames[0], L"subtraction") == 0)
    {
        rgDispId[0] = 3;
    }

    else if (wcscmp(rgszNames[0], L"exponentiation") == 0)
    {
        rgDispId[0] = 4;
    }
//proMatrix
    else if (wcscmp(rgszNames[0], L"find_determinant") == 0)
    {
        rgDispId[0] = 5;
    }

    else if (wcscmp(rgszNames[0], L"transposition") == 0)
    {
        rgDispId[0] = 6;
    }

    else if (wcscmp(rgszNames[0], L"rank") == 0)
    {
        rgDispId[0] = 7;
    }

    else if (wcscmp(rgszNames[0], L"reverse") == 0)
    {
        rgDispId[0] = 8;
    }

    else if (wcscmp(rgszNames[0], L"minor") == 0)
    {
        rgDispId[0] = 9;
    }

    else if (wcscmp(rgszNames[0], L"check") == 0)
    {
        rgDispId[0] = 10;
    }

    else
    {
        return E_NOTIMPL;
    }

    return S_OK;
}

HRESULT Server::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pDispParams,VARIANT* pVarResult,
                                    EXCEPINFO* pExcepInfo, UINT* puArgErr)
{
    if (dispIdMember == 1)
    {
        std::cout<<"mult_num"<<std::endl;
        mult_num();
    }
    else if (dispIdMember == 2)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());
        
        int isTrue = 0;
        addition();

        if (wFlags == 3)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = isTrue;
        }
    }
    else if (dispIdMember == 3)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());

        HRESULT res = subtraction();

        if (wFlags == 4)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = SUCCEEDED(res) ? true : false;
        }
    }
    else if (dispIdMember == 4)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());

        HRESULT res = exponentiation();

        if (wFlags == 5)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = SUCCEEDED(res) ? true : false;
        }
    }
    //promatrix
    else if (dispIdMember == 5)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());
        HRESULT res = find_determinant();

        if (wFlags == 6)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = SUCCEEDED(res) ? true : false;
        }
    }
    else if (dispIdMember == 6)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());
        HRESULT res = transposition();

        if (wFlags == 7)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = SUCCEEDED(res) ? true : false;
        }
    }
    else if (dispIdMember == 7)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());
        HRESULT res = rank();

        if (wFlags == 8)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = SUCCEEDED(res) ? true : false;
        }
    }
    else if (dispIdMember == 8)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());

        HRESULT res = reverse();

        if (wFlags == 9)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = SUCCEEDED(res) ? true : false;
        }
    }
    else if (dispIdMember == 9)
    {
        DISPPARAMS param = *pDispParams;
        VARIANT arg = (param.rgvarg)[0];
        std::wstring ws(arg.bstrVal, SysStringLen(arg.bstrVal));
        std::string s(ws.begin(), ws.end());

        HRESULT res = minor();

        if (wFlags == 10)
        {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = SUCCEEDED(res) ? true : false;
        }
    }
    else if (dispIdMember == 10)
    {
        if ( (wFlags==DISPATCH_PROPERTYGET) || (wFlags==1) || (wFlags==3) )
       {
          if (pVarResult!=NULL)
          {
            pVarResult->vt = VT_INT;
            pVarResult->intVal = check; 
          }
       }
       else if (wFlags==DISPATCH_PROPERTYPUT)
       {
          DISPPARAMS param = *pDispParams;
          VARIANT arg = (param.rgvarg)[0];
          VariantChangeType(&arg,&arg,0,VT_INT);
          check = arg.intVal;
       }
       else
       {
         return E_NOTIMPL;
       }
    }
    else
    {
        return E_NOTIMPL;
    }

    return S_OK;
}

HRESULT Server::GetTypeInfoCount(UINT* pctinfo){
    return S_OK;
}

HRESULT Server::GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo** ppTInfo){
    return S_OK;
}
//IDispatch_end
Server::~Server(){
    cout<<"Server::~Server()"<<endl;
}

//DefaultMatrixClassFactory__________________________________________________________________________________

MatrixClassFactory::MatrixClassFactory(){
    cout << "MatrixClassFactory::MatrixClassFactory" << endl;
    this->refCount = 0;
    }


HRESULT MatrixClassFactory::QueryInterface(const IID& iid, void** object){
    cout<<"MatrixClassFactory::QueryInterface"<<endl;
        if (iid == Constants::IID_IClassFactory)
        {
          *object = (IUnknown*) (IClassFactory*) this;
        }
        else if (iid == Constants::IID_IMatrixClassFactory){
            *object = (IUnknown*) (IMatrixClassFactory*) this;
        }
        else{
            *object = NULL;
            return E_NOINTERFACE;
        }

        this->AddRef();
        return S_OK;
}

ULONG MatrixClassFactory::AddRef(){
    refCount++;
    return refCount;
}

ULONG MatrixClassFactory::Release(){
    refCount--;
    if (refCount == 0){
        delete this;
    }
    return refCount;
}

HRESULT MatrixClassFactory::CreateInstance(IUnknown* pUnknownOuter,const IID& iid, void** object){
    cout<<"MatrixClassFactory::CreateInstance"<<endl;

    if (pUnknownOuter != NULL){
        return E_NOTIMPL;
    }
    IUnknown* s = (IUnknown*) (IDefaultMatrix*) new Server();

    HRESULT res = s->QueryInterface(iid, object);

    return res;
}

HRESULT MatrixClassFactory::CreateInstance(const IID& iid, void** object, int license[]){
    cout<<"MatrixClassFactory::CreateInstance with license"<<endl;
    HRESULT res = E_NOTIMPL;

    if (license != NULL){
        IUnknown*s = (IUnknown*)(IDefaultMatrix*) new Server();

        res = s->QueryInterface(iid, object);
    }
    return res;
}

HRESULT MatrixClassFactory::LockServer(BOOL bLock){
    return S_OK;
}

MatrixClassFactory::~MatrixClassFactory(){
    cout<<"MatrixClassFactory::~MatrixClassFactory"<<endl;
}

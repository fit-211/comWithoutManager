#pragma once

#include <string>
#include <vector>
#include "windows.h"
#include <iostream>
#include <objbase.h>

class Constants2
{
    public:
     static IID IID_IOneMatrix;
     static IID IID_IMatrixManagerClassFactory;

     static CLSID CLSID_MatrixManager;
};

class IOneMatrix : public IUnknown
{
    public:
        virtual HRESULT __stdcall setMatrix() = 0;
};

class IMatrixManagerClassFactory : public IUnknown
{
    public:
        virtual HRESULT __stdcall CreateInstance(const IID& iid, void** object, int license[]) = 0;
};

extern "C" HRESULT __stdcall __declspec(dllexport) DllGetClassObject(const CLSID& clsid, const IID& iid, void** object);

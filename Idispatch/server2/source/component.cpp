#include "component.h"
using namespace std;

IID Constants::IID_IUnknown = {0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};
IID Constants::IID_IClassFactory = {0x00000001,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};

IID Constants::IID_IDefaultMatrix = {0x00000001,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IProMatrix = {0x00000002,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IMatrixClassFactory = {0x00000011,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};

//781C3DFD-8C32-4991-A6BC-5212FEB952B1
CLSID Constants::CLSID_Server = {0x781C3DFD,0x8C32,0x4991,{0xA6,0xBC,0x52,0x12,0xFE,0xB9,0x52,0xB1}};

IID Constants2::IID_IMatrixManagerClassFactory = {0x00000012,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants2::IID_IOneMatrix = {0x00000003,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};

//{847390d0-6af5-4d19-9159-c91516bc3555}
CLSID Constants2::CLSID_MatrixManager = {0x847390d0,0x6af5,0x4d19,{0x91,0x59,0xc9,0x15,0x16,0xbc,0x35,0x55}};

MatrixManager::MatrixManager(){
    cout << "MatrixManager::MatrixManager" << endl;

    refCount = 0;

    CoInitialize(NULL);

    IClassFactory* MCF = NULL;

    HRESULT resFactory = CoGetClassObject(Constants::CLSID_Server, CLSCTX_INPROC_SERVER, NULL, Constants::IID_IClassFactory, (void**) &MCF);

    cout << "matrix manager" << endl;

    if(!SUCCEEDED(resFactory)){
        cout << "No Factory" << endl;
    }

    HRESULT resInstance = MCF->CreateInstance(NULL, Constants::IID_IDefaultMatrix, (void**) &dm);

    if (!SUCCEEDED(resInstance)){
        cout << "No instance" << endl;
    }

    HRESULT resQuery = dm->QueryInterface(Constants::IID_IProMatrix, (void**) &pm);

    if (!SUCCEEDED(resQuery)){
        cout << "No QueryInterface" << endl;
    }

    MCF->Release();

    CoUninitialize();
}

HRESULT MatrixManager::QueryInterface(const IID& iid, void** object){
   cout << "MatrixManager QueryInterface" << endl;

   if (iid == Constants::IID_IUnknown){
     *object = (IUnknown*) (IDefaultMatrix*) this;
   }
   else if (iid == Constants::IID_IDefaultMatrix){
     *object = static_cast<IDefaultMatrix*>(this);
   }
   else if (iid == Constants::IID_IProMatrix){
     *object = static_cast<IProMatrix*>(this);
   }
   else if (iid == Constants2::IID_IOneMatrix){
    *object = static_cast<IOneMatrix*>(this);
   }
   else{
     *object = NULL;
     return E_NOINTERFACE;
   }

   this->AddRef();
   return S_OK;
}

ULONG MatrixManager::AddRef(){
    refCount++;
    return refCount;
}

ULONG MatrixManager::Release(){
    refCount--;
    if (refCount == 0)
    {
        delete this;
    }
    
    return refCount;
}


HRESULT MatrixManager::setMatrix(){	 	    		
  cout << "MatrixManager set matrix" << endl;
  int N;
  int M;
  cin >> M;
  cin >> N;
	int matrix [M][N];
  for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++)
			cin >> matrix[i][j];
	};
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++){
			cout << matrix[i][j];
			};
		cout << " " << endl;
	}; 	   
	//return matrix[M][N];
  return S_OK;
}

HRESULT MatrixManager::mult_num(){
  cout << "mult sum" << endl;
  return dm->mult_num();
}

HRESULT MatrixManager::addition(){	 	    		
  return dm->addition();
}

HRESULT MatrixManager::subtraction(){	 	
  cout << "Server::subtraction" << endl;    		
  return dm->subtraction();
}

HRESULT MatrixManager::exponentiation(){	 	    		
  return dm->exponentiation();
}

HRESULT MatrixManager::transposition(){	 	    		
  return pm->transposition();
}

HRESULT MatrixManager::rank(){	 	    		
  return pm->rank();
}

HRESULT MatrixManager::reverse(){	 	    		
  return pm->reverse();
}

HRESULT MatrixManager::find_determinant(){
  return pm->find_determinant();
}

HRESULT MatrixManager::minor(){	 	    		
  return pm->minor();
}

MatrixManager::~MatrixManager(){
    cout<<"MatrixManager::~MatrixManager()"<<endl;
    dm->Release();
    pm->Release();
}

//MatrixManagerClassFactory____________________________________________________________________________________________________

MatrixManagerClassFactory::MatrixManagerClassFactory(){
    cout<<"MatrixManagerClassFactory::MatrixManagerClassFactory"<<endl;
    this->refCount = 0;
}


HRESULT MatrixManagerClassFactory::QueryInterface(const IID& iid, void** object){
    cout<<"MatrixManagerClassFactory::QueryInterface"<<endl;
        if (iid == Constants::IID_IClassFactory){
            *object = (IUnknown*) (IClassFactory*) this;
        }
        else if (iid == Constants2::IID_IMatrixManagerClassFactory){
            *object = (IUnknown*) (IMatrixManagerClassFactory*) this;
        }
        else{
            *object = NULL;
            return E_NOINTERFACE;
        }

        this->AddRef();
        return S_OK;
}

ULONG MatrixManagerClassFactory::AddRef(){
    refCount++;
    return refCount;
}

ULONG MatrixManagerClassFactory::Release(){
    refCount--;
    if (refCount == 0){
        delete this;
    }
    return refCount;
}

HRESULT MatrixManagerClassFactory::CreateInstance(IUnknown* pUnknownOuter, const IID& iid, void** object){
    cout<<"MatrixManagerClassFactory::CreateInstance"<<endl;

    IUnknown* s = (IUnknown*) (IDefaultMatrix*) new MatrixManager();

    HRESULT res = s->QueryInterface(iid, object);

    return res;
}

HRESULT MatrixManagerClassFactory::CreateInstance(const IID& iid, void** object, int license[]){
    cout<<"MatrixManagerClassFactory::CreateInstance with license"<<endl;
    HRESULT res = E_NOTIMPL;

    if (license != NULL){
        IUnknown*s = (IUnknown*) (IDefaultMatrix*) new MatrixManager();

        res = s->QueryInterface(iid, object);
    }
    return res;
}

HRESULT MatrixManagerClassFactory::LockServer(BOOL bLock){
    return S_OK;
}

MatrixManagerClassFactory::~MatrixManagerClassFactory(){
    cout<<"MatrixManagerClassFactory::~MatrixManagerClassFactory"<<endl;
}

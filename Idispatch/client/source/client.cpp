#include "lib/interface.h"
#include "lib/interface2.h"
#include <objbase.h>

#include <iostream>

const IID IID_NULL_ = {0x00000000,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};

IID Constants::IID_IUnknown = {0x00000000,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};
IID Constants::IID_IClassFactory = {0x00000001,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};

IID Constants::IID_IDefaultMatrix = {0x00000001,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IProMatrix = {0x00000002,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IMatrixClassFactory = {0x00000011,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants::IID_IDispatch = {0x00020400,0x0000,0x0000,{0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x46}};

//781C3DFD-8C32-4991-A6BC-5212FEB952B1
CLSID Constants::CLSID_Server = {0x781C3DFD,0x8C32,0x4991,{0xA6,0xBC,0x52,0x12,0xFE,0xB9,0x52,0xB1}};

IID Constants2::IID_IMatrixManagerClassFactory = {0x00000012,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};
IID Constants2::IID_IOneMatrix = {0x00000003,0x0000,0x0000,{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00}};

//{847390d0-6af5-4d19-9159-c91516bc3555}
CLSID Constants2::CLSID_MatrixManager = {0x847390d0,0x6af5,0x4d19,{0x91,0x59,0xc9,0x15,0x16,0xbc,0x35,0x55}};

using namespace std;

int main() {
	CoInitialize(NULL);

    CLSID CLSID_Server;
    {
        const wchar_t* progID = L"SDV.Application";

        HRESULT resCLSID = CLSIDFromProgID(progID, &CLSID_Server);
        
        if (!SUCCEEDED(resCLSID))
        {
            std::cout<<"NO CLSID"<<std::endl;
        }
    }

    cout<<"try client dll"<<endl;
    IClassFactory* MCF = NULL;
    HRESULT resFactory = CoGetClassObject(Constants::CLSID_Server, CLSCTX_INPROC_SERVER, NULL, Constants::IID_IClassFactory, (void**) &MCF);

    if(!SUCCEEDED(resFactory)){
        cout << "No Factory" << endl;
    }

    IDefaultMatrix* dm = NULL;
    HRESULT resInstance = MCF->CreateInstance(NULL, Constants::IID_IDefaultMatrix, (void**) &dm);
    if (!SUCCEEDED(resInstance))
    {
        std::cout<<"No instance"<<std::endl;
    }

    IProMatrix* pm = NULL;
    HRESULT resQuery = dm->QueryInterface(Constants::IID_IProMatrix, (void**) &pm);
    if (!SUCCEEDED(resQuery))
    {
        std::cout<<"No query"<<std::endl;
    }

    IDispatch* pDisp = NULL;
    HRESULT resQueryDisp = dm->QueryInterface(Constants::IID_IDispatch,(void**)&pDisp);
    if (!(SUCCEEDED(resQueryDisp)))
    {
        std::cout<<"No query dispatch";
    }

    DISPID dispid;
    int namesCount = 1;
    OLECHAR** names = new OLECHAR*[namesCount];
    OLECHAR* name = const_cast<OLECHAR*>(L"mult_num");
    names[0] = name;
    HRESULT resID_Name = pDisp->GetIDsOfNames(
                                                    IID_NULL_,
                                                    names,
                                                    namesCount,
                                                    GetUserDefaultLCID(),
                                                    &dispid
                                               );
    if (!(SUCCEEDED(resID_Name)))
    {
        std::cout<<"No id of name"<<std::endl;
    }
    else
    {
        DISPPARAMS dispparamsNoArgs = {
                                        NULL,
                                        NULL,
                                        0,
                                        0,
                                    };

        HRESULT resInvoke = pDisp->Invoke(
                                            dispid, // DISPID
                                            IID_NULL_,
                                            GetUserDefaultLCID(),
                                            DISPATCH_METHOD,
                                            &dispparamsNoArgs,
                                            NULL,
                                            NULL,
                                            NULL
                                        );
        if (!(SUCCEEDED(resInvoke)))
        {
          std::cout<<"No invoke"<<std::endl;
        }
      }

      MCF->Release();
      dm->Release();
      pm->Release();
      pDisp->Release();

    CoUninitialize();
	return 0;
}